package com.epam.utilities;

public class Const {

    public static int getStartCycle() {
        return 0;
    }

    public static int getTailCount() {
        return 1;
    }

    public static int getHeadCount() {
        return -1;
    }

    public static int getResizeMin() {
        return 2;
    }

    public static int getResizeMax() {
        return 4;
    }
}
