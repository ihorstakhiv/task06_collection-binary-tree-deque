package com.epam.controllers;

import com.epam.models.DequeModel;

public class ControllerDeque<V> implements Controller {

    private DequeModel<V> deque = new DequeModel<>();

    public void addHeadElement(V value) {
        deque.addFirst(value);
        deque.print();
    }

    public void addLastElement(V value) {
        deque.addLast(value);
        deque.print();
    }

    public void deleteFirstElement() {
        deque.removeFirst();
        deque.print();
    }

    public void deleteLastElement() {
        deque.removeLast();
        deque.print();
    }

    public int dequeSize() {
        return deque.size();
    }

    public void showDeque() {
        deque.print();
    }
}

