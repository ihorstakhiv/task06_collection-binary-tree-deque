package com.epam.controllers;

import com.epam.models.BinaryTreeModel;

public class ControllerBinaryTree<K, V> implements Controller {

    private BinaryTreeModel<K, V> binaryTree = new BinaryTreeModel<>();

    public void putInBinaryTree(K key, V value) {
        binaryTree.put(key, value);
    }

    public void printBinaryTree() {
        binaryTree.showThisMap();
    }

    public void removeElement(K key) {
        binaryTree.delete(key);
    }
}
