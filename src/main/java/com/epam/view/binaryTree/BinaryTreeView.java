package com.epam.view.binaryTree;

import com.epam.controllers.ControllerBinaryTree;
import com.epam.view.AbstractView;
import com.epam.view.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class BinaryTreeView implements AbstractView {

    private ControllerBinaryTree<Object, Object> controller= new ControllerBinaryTree<>();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private static Logger logger = LogManager.getLogger(BinaryTreeView.class);

    public BinaryTreeView() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\n  1 - Fill out a binary map (key - value)");
        menu.put("2", "\n  2 - Delete element (key)");
        menu.put("3", "\n  3 - Show binary map");
        menu.put("Q", "\n  Q - exit\n");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::fillOutBinaryMap);
        methodsMenu.put("2", this::deleteElement);
        methodsMenu.put("3", this::showBinaryMapMenu);
    }

    private void fillOutBinaryMap() {
        logger.info("\nPut key :");
        Object key = input.next();
        logger.info("\nPut value :");
        Object value = input.next();
        controller.putInBinaryTree(key,value);
        showBinaryMap();
        logger.info("\nIf you want to come back please put any letter else put 'N' ");
        String choose = input.next();
        if (choose.equals("N")||choose.equals("n")){
            fillOutBinaryMap();
        }
    }

    private void deleteElement() {
        logger.info("Set element key :");
        String variable = input.next();
        controller.removeElement(variable);
        showBinaryMap();
        logger.info("\nIf you want to come back please put any letter else put 'N' ");
        String choose = input.next();
        if (choose.equals("N")||choose.equals("n")){
            deleteElement();
        }
    }

    private void showBinaryMap() {
        logger.info("\nYour map:\n********************\n");
        logger.info("Keys:         Values:\n");
        controller.printBinaryTree();
        logger.info("\n********************\n");
    }

    private void showBinaryMapMenu(){
        showBinaryMap();
        logger.info("\nIf you want to come back please put anything");
        input.next();
    }

        private void outputMenu() {
        logger.info("\n**************       MENU       ***************\n");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
               e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
