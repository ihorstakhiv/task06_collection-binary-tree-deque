package com.epam.view.deque;

import com.epam.controllers.ControllerDeque;
import com.epam.view.AbstractView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

public class DequeView<V> implements AbstractView {

    private static Logger logger = LogManager.getLogger(DequeView.class);
    private static Scanner input = new Scanner(System.in);

    private ControllerDeque<String> controller = new ControllerDeque<>();

    public void show() {
        menu();
        logger.info("\nChoose your choice: \n");
        switch (input.nextInt()) {
            case 1:
                controller.addHeadElement(setValue());
                mainOptions();
                break;
            case 2:
                controller.addLastElement(setValue());
                mainOptions();
                break;
            case 3:
                controller.deleteFirstElement();
                mainOptions();
                break;
            case 4:
                controller.deleteLastElement();
                mainOptions();
            case 5:
                logger.info(controller.dequeSize());;
                mainOptions();
                break;
            case 6:
                controller.showDeque();
                mainOptions();
                break;
            case 7:
                logger.info("\nGoodbye.\n");
                break;
            default:
                logger.error("\nSelection out of range. Try again\n");
                mainOptions();
                break;
        }
    }

    private static void menu() {
        logger.info("\n********      MENU      *******\n");
        logger.info("\n" + "1  -" + DequeEnum.FIRST_POSITION.getMeaning());
        logger.info("\n" + "2  -" + DequeEnum.SECOND_POSITION.getMeaning());
        logger.info("\n" + "3  -" + DequeEnum.THIRD_POSITION.getMeaning());
        logger.info("\n" + "4  -" + DequeEnum.FOURTH_POSITION.getMeaning());
        logger.info("\n" + "5  -" + DequeEnum.FIFTH_POSITION.getMeaning());
        logger.info("\n" + "6  -" + DequeEnum.SIXTH_POSITION.getMeaning());
        logger.info("\n" + "7  -" + DequeEnum.EXIT_POSITION.getMeaning()+"\n");
    }

    private String setValue() {
        logger.info("\n Please set your value: \n");
        return input.next();
    }
    private void previousMenu(){
        logger.info("\n Please set any value if you want come back: \n");
        input.next();
    }

    private void mainOptions(){
        previousMenu();
        show();
    }
}
