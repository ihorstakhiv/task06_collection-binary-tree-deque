package com.epam.view.deque;

public enum DequeEnum {

    FIRST_POSITION("Fill out first element"), SECOND_POSITION("Fill out last element"), THIRD_POSITION("Delete first element"), FOURTH_POSITION("Delete last element"),
    FIFTH_POSITION("Size of Deque"), SIXTH_POSITION("Show Deque"), EXIT_POSITION("EXIT");

    private String meaning;

    DequeEnum(String meaning) {
        this.meaning = meaning;
    }

    public String getMeaning() {
        return meaning;
    }
}
