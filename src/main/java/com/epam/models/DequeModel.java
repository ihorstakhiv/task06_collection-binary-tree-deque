package com.epam.models;

import com.epam.utilities.Const;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class DequeModel<Type> implements Iterable<Type> {

    private Type[] deckArray;
    private int dequeSize;
    private int head;
    private int tail;

    public DequeModel() {
        deckArray = (Type[]) new Object[Const.getResizeMin()];
        dequeSize = Const.getStartCycle();
        head = Const.getStartCycle();
        tail = deckArray.length - Const.getTailCount();
    }

    private boolean isEmpty() {
        return dequeSize == Const.getStartCycle();
    }

    public int size() {
        return dequeSize;
    }

    public void addFirst(Type type) {
        if (dequeSize == deckArray.length) resize(Const.getResizeMin() * deckArray.length);
        deckArray[head--] = type;
        if (head == Const.getStartCycle()) head = deckArray.length - Const.getTailCount();
        dequeSize++;
    }

    public void addLast(Type type) {
        if (dequeSize == deckArray.length) resize(Const.getResizeMin() * deckArray.length);
        deckArray[tail++] = type;
        if (tail == deckArray.length) tail = Const.getStartCycle();;
        dequeSize++;
    }

    public void removeFirst() {
        if (isEmpty()) throw new NoSuchElementException("Deck is already empty!");
        if (head == tail - Const.getTailCount()) head = Const.getHeadCount();
        Type type = deckArray[++head];
        deckArray[head] = null;
        dequeSize--;
        if (dequeSize > Const.getStartCycle() && dequeSize == deckArray.length / Const.getResizeMax()) resize(deckArray.length / Const.getResizeMin());
    }

    public void removeLast() {
        if (isEmpty()) throw new NoSuchElementException("Deck is already empty!");
        if (tail == Const.getStartCycle()) tail = deckArray.length;
        Type type = deckArray[--tail];
        deckArray[tail] = null;
        dequeSize--;
        if (dequeSize > Const.getStartCycle() && dequeSize == deckArray.length / Const.getResizeMax()) resize(deckArray.length / Const.getResizeMin());
    }

    private void resize(int size) {
        Type[] newDeck = (Type[]) new Object[size];
        for (int i = Const.getStartCycle(); i < dequeSize; i++) {
            newDeck[i] = deckArray[(head + Const.getTailCount() + i) % deckArray.length];
        }
        deckArray = newDeck;
        head = deckArray.length - Const.getTailCount();
        tail = dequeSize;
    }

    public Iterator<Type> iterator() {
        return new ArrayIterator();
    }

    public void print() {
        for (int i = Const.getStartCycle(); i < deckArray.length; i++) {
            System.out.println(deckArray[i]);
        }
    }

    private class ArrayIterator implements Iterator<Type> {

        private int current = Const.getStartCycle();

        public boolean hasNext() {
            return current < dequeSize;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Type next() {
            if (!hasNext()) throw new NoSuchElementException();
            Type type = deckArray[(head + Const.getTailCount() + current) % deckArray.length];
            current++;
            return type;
        }
    }
}
